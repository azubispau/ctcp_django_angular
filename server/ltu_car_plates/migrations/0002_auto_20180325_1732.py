# Generated by Django 2.0.3 on 2018-03-25 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ltu_car_plates', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ltucarplates',
            name='description',
            field=models.CharField(max_length=64),
        ),
        migrations.AlterField(
            model_name='ltucarplates',
            name='name',
            field=models.CharField(max_length=64),
        ),
    ]

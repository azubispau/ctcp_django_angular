from rest_framework import viewsets
from ltu_car_plates.models import LtuCarPlates
from ltu_car_plates.serializers import (LtuCarPlatesSerializer)


class LtuCarPlatesViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing ltu_car_plats objects """
    queryset = LtuCarPlates.objects.all()
    serializer_class = LtuCarPlatesSerializer

	# @detail_route(url_name='delete_plate', url_path='delete_plate/(?P<plate_id>[0-9]+)')
	# def delete_plate(self, request, pk=None, plate_id=None):
	# 	print ("hoh")
	# 	CarPlateA.objects.filter(id=plate_id).delete()
	# 	queryset = LtuCarPlates.objects.all()
	# 	serializer = LtuCarPlatesSerializer(queryset, context={'request': request})
	# 	return Response(serializer.data)

from django.db import models

class LtuCarPlates(models.Model):
    """ High-level ltu car platees LtuCarPlates model"""
    plate_number = models.CharField(max_length=6, default="ABC123")
    plate_owner = models.CharField(max_length=64, default="Jonas Jonaitis")

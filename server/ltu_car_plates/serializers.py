from rest_framework import serializers
from ltu_car_plates.models import LtuCarPlates


class LtuCarPlatesSerializer(serializers.ModelSerializer):
    """ Serializer to represent the ltu_car_plats model """
    class Meta:
        model = LtuCarPlates
        fields = ("plate_number", "plate_owner", 'id')

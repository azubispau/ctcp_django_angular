"""my_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter
from ltu_car_plates.views import LtuCarPlatesViewSet

# from ltu_car_plates import views

router = DefaultRouter()
router.register(prefix='ltu_car_plates', viewset=LtuCarPlatesViewSet)
# router.register(prefix='(?P<id>\d+)/delete_plate', viewset=LtuCarPlatesViewSet)

urlpatterns = router.urls

urlpatterns += [
	path('admin/', admin.site.urls),
	# path('<int:plate_id>/delete_plate', views.delete_plate, name='delete_plate'),
]
ltu_car_plates
    .factory('LtuCarPlates', function($resource) {
        return $resource(
            'http://localhost:8000/ltu_car_plates/:id/',
            {},
            {
                'query': {
                    method: 'GET',
                    isArray: true,
                    headers: {
                        'Content-Type':'application/json'
                        //'Access-Control-Allow-Origin':'*'
                    }
                }
            },
            {
                stripTrailingSlashes: false
            }
        );
    });
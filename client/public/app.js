'use strict';

var ltu_car_plates = angular.module("ltu_car_plates", [
        'ngResource'
    ]);

angular
    .module('SampleApplication', [
        'appRoutes',
        'ltu_car_plates'
    ]);
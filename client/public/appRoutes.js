angular
    .module('appRoutes', ["ui.router"])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider.state({
        name: 'ltu_car_plates',
        url: '/',
        templateUrl: 'public/components/ltu_car_plates/templates/ltu_car_plates.template',
        controller: 'LtuCarPlatesController'
    });

    $urlRouterProvider.otherwise('/');
}]);
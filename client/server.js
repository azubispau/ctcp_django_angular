var express = require('express');
var server = express();
server.use(express.static(__dirname));

var port = process.env.PORT || 8081;
server.listen(port);
console.log('Clinet side is running on Port: ' + port);

exports = module.exports = server;
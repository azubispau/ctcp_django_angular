# Git remote : git clone https://enigmq@bitbucket.org/enigmq/ctcp_django_angular.git
# pull/push password: WrX2FJdWeVTSq66B8nFP

# Note: before pulling project you should probably create virtual env.
# How to Create virtual env:
# open terminal, head to directory that you wish your virtual env to appear at, and type:
	virtualenv my_project
# change directory to my_project and type:
	source bin/activate
# this will activate virtual environment
# Install django (if you dont have django. If you dont have pip you might want to install it will sudo apt-get install pip):
	pip3 install django
# Instaal Django res framework:
	pip3 install djangorestframework
# Let's Create another directory:
	mkdir my_project
	cd ltcp
# Now lets initialize directory as git remote by typing:
	git init
# Add remote: (not nessesary)
	git remote add origin https://enigmq@bitbucket.org/enigmq/ctcp_django_angular.git
# Check your remote list:
	git remote -v
# Now we can pull:
	git pull https://enigmq@bitbucket.org/enigmq/ctcp_django_angular.git
# Enter Password:
	WrX2FJdWeVTSq66B8nFP

# After succesful pull change directory one more time:
	cd server
# Now we can launch our projects back-end with command:
	python3 manage.py runserver

# open another terminal and head to your project directory. This time client.
# If you don't have notejs you might want to get it ( command 'sudo apt-get install npm' might help). Type:
	npm install
	npm install -g bower
	sudo bower install --config.interactive=false --allow-root
	node server.js

# Now our clinet side should be up and running on port 8081

# after launching project go to: http://127.0.0.1:8000/ltu_car_plates/ to view List or car plates.

# Project has superuser for admin access:
	http://127.0.0.1:8000/admin/
	username : admin
	password : adminadmin

# Project is built with at that time latest version: django 2.0.3
# by Paulius Azubalis
# paulius.azubalis@gmail.com